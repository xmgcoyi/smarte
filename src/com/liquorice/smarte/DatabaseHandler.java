package com.liquorice.smarte;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper {

	// All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;
    //Database Name
    private static final String DATABASE_NAME = "contactsManager.db";
    // Contacts table name
    private static final String TABLE_CONTACTS = "contacts";
    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_SURNAME = "surname";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_MOBILE = "mobile";
    private static final String KEY_DOB = "dob";
    private static final String KEY_GENDER = "gender";
    private static final String KEY_REGION = "region";
    
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_SURNAME + " TEXT, " + KEY_EMAIL + " TEXT, " 
                + KEY_MOBILE + " TEXT, "+ KEY_DOB + " TEXT, " 
                + KEY_GENDER + " TEXT, "+ KEY_REGION + " TEXT"
                + ")";        
        db.execSQL(CREATE_CONTACTS_TABLE);
    }  

    public int addContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
     
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getName());
        values.put(KEY_SURNAME, contact.getSurname());
        values.put(KEY_EMAIL, contact.getEmail());
        values.put(KEY_MOBILE, contact.getMobile());
        values.put(KEY_DOB, contact.getDob());
        values.put(KEY_GENDER, contact.getGender());
        values.put(KEY_REGION, contact.getRegion());
        
        if(!isDuplicateContact(contact)) {
        	// Inserting Row
            int id = (int) db.insert(TABLE_CONTACTS, null, values);
            db.close(); // Closing database connection
            return id;
        }
        return -1;
    }
    
    public Cursor findAllContacts() { 
    	String[] fields =  new String[]{ KEY_ID, KEY_NAME, KEY_SURNAME, KEY_EMAIL, KEY_MOBILE, KEY_DOB, KEY_GENDER, KEY_REGION};
    	Cursor cursor = this.getReadableDatabase().query(TABLE_CONTACTS, fields, null, null, null, null, null);
    	return cursor;
   }
    public Contact[] findAllContactObjects() { 
    	Cursor cursor = this.findAllContacts();
        int numRows = cursor.getCount();       
        
        if(numRows > 0) {   
        	Contact[] contacts = new Contact[numRows];
        	
        	int nameIndex = cursor.getColumnIndex(Contact.KEY_NAME);
        	int surnameIndex = cursor.getColumnIndex(Contact.KEY_SURNAME);
        	int emailIndex = cursor.getColumnIndex(Contact.KEY_EMAIL);
        	int mobileIndex = cursor.getColumnIndex(Contact.KEY_MOBILE);
        	int dobIndex = cursor.getColumnIndex(Contact.KEY_DOB);
        	int genderIndex = cursor.getColumnIndex(Contact.KEY_GENDER);
        	int regionIndex = cursor.getColumnIndex(Contact.KEY_REGION);
        	
	      	  for(int i = 0; i < numRows; i++) {
	      		cursor.moveToPosition(i);
	      		
	      		contacts[i] = new Contact(
	      				cursor.getString(nameIndex),
	      				cursor.getString(surnameIndex),
	      				cursor.getString(emailIndex),
	      				cursor.getString(mobileIndex),
	      				cursor.getString(dobIndex),
	      				cursor.getString(genderIndex),
	      				cursor.getString(regionIndex)
	      		);	      		  
	      	  }
	      	  return contacts;
        }
        return null;
   }

    public int deleteContact(Contact contact) {
    	 String whereClause = KEY_EMAIL+"= ? AND "+KEY_SURNAME+"= ? AND "+KEY_NAME+"=? ";
    	 String[] whereArgs = new String[]{contact.getEmail(), contact.getSurname(), contact.getName()};
    	 Log.i("Liquorice", "whereClause+"+whereClause);
    	 int affectedRows = this.getWritableDatabase().delete(TABLE_CONTACTS, whereClause, whereArgs);
    	 return affectedRows;
    }
    
    public boolean isDuplicateContact(Contact contact) {
    	SQLiteDatabase db = this.getReadableDatabase();
    	String[] strAr = new String[]{KEY_EMAIL, KEY_SURNAME, KEY_NAME};    	
    	String[] args = new String[]{contact.getEmail(), contact.getSurname(), contact.getName()};
    	
    	String selection = KEY_EMAIL+"= ? AND "+KEY_SURNAME+"= ? AND "+KEY_NAME+"=? ";
    	
    	Log.i("Liquorice", "quetying... ");    
    	Cursor cursor = db.query( TABLE_CONTACTS, strAr, selection, args, null, null, null);
    	Log.i("Liquorice", "row count:" + cursor.getCount());
    	if(cursor.getCount() > 0) {
    		return true;
    	}
//query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
///public Cursor query (String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy, String limit)
    	return false;
    }
    
	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}
}