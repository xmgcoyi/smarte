package com.liquorice.smarte;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkStateReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
//     super.onReceive(context, intent);
	     Log.i("Liquorice","Network connectivity change");
	     if(intent.getExtras()!=null) {
	        NetworkInfo ni=(NetworkInfo) intent.getExtras().get(ConnectivityManager.EXTRA_NETWORK_INFO);
	        if(ni!=null && ni.getState()==NetworkInfo.State.CONNECTED) {
	        	//Check if Database if has data and sync
	        	 Intent myIntent = new Intent(context,ContactSendService.class);        
	             context.startService(myIntent);	             
	            Log.i("Liquorice","Network "+ni.getTypeName()+" connected");
	            
	        } else if(intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY,Boolean.FALSE)) {
	            Log.i("Liquorice","There's no network connectivity");
	        }
	   }
	}
}
    
