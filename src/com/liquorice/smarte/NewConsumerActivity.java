package com.liquorice.smarte;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class NewConsumerActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_consumer);

	      Contact[] contacts = new DatabaseHandler(getBaseContext()).findAllContactObjects();
	      if(contacts != null) {
	    	  LinearLayout offlineDataLayout = (LinearLayout) findViewById(R.id.offlineDataLayout);
	    	  offlineDataLayout.setVisibility(View.VISIBLE);
	      }
	}
	
	public void newConsumerButtonClick(View view) {
		Intent intent = new Intent(this, SmartE.class);
		startActivity(intent);
	}
}
