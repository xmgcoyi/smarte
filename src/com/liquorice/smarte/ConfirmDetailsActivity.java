package com.liquorice.smarte;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class ConfirmDetailsActivity extends Activity {

	TextView nameTextView;
	TextView surnameTextView;
	TextView emailTextView;
	TextView mobileTextView;
	TextView genderTextView;
	TextView regionTextView;
	
	TextView ddTextView;
	TextView mmTextView;
	TextView yyyyTextView;
	Contact contact;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_confirm_details);		
		//init views
		nameTextView = (TextView) findViewById(R.id.nameTextView);
		surnameTextView = (TextView) findViewById(R.id.surnameTextView);
		emailTextView = (TextView) findViewById(R.id.emailTextView);		
		mobileTextView = (TextView) findViewById(R.id.mobileTextView);
		genderTextView = (TextView) findViewById(R.id.genderTextView);
		regionTextView = (TextView) findViewById(R.id.regionTextView);
		ddTextView = (TextView) findViewById(R.id.ddTextView);
		mmTextView = (TextView) findViewById(R.id.mmTextView);
		yyyyTextView = (TextView) findViewById(R.id.yyyyTextView);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    contact = (Contact) extras.getParcelable("contact");
		    String ddString = extras.getString("dd");
		    String mmString = extras.getString("mm");
		    String yyyyString = extras.getString("yyyy");

		    nameTextView.setText(contact.getName());
		    surnameTextView.setText(contact.getSurname());
		    emailTextView.setText(contact.getEmail());
		    mobileTextView.setText(contact.getMobile());
		    genderTextView.setText(contact.getGender());
		    regionTextView.setText(contact.getRegion());
		    
		    ddTextView.setText(ddString);
		    mmTextView.setText(mmString);
		    yyyyTextView.setText(yyyyString);
		}		
	}
	
	public void backButtonClick(View view) {
		finish();
	}
	
	public void submitButtonClick(View view) {
		DatabaseHandler dbHandler = new DatabaseHandler(this);
		//send details only when connected
		if(isConnected()) {										
			new SweetSpotApi(getBaseContext(), contact).execute(SweetSpotApi.getPostUrl());
		}else {
			dbHandler.addContact(contact);
		} 
		//Thank you page
		Intent intent = new Intent(this, ThankYouActivity.class);
		startActivity(intent);
	}

    
    /**
     * Checks connection
     * @return boolean
     */
    private boolean isConnected() {
    	ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);				 
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();				
		boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
		return isConnected;
    }  
}
