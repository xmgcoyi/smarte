package com.liquorice.smarte;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

public class SweetSpotApi extends AsyncTask<String, String, String> {
//This class might be inappropriately named. Please feel free to refactor if you can :)	

	private static final String BASE_URL = "http://sweetspot.distell.co.za";
	private static final String CONTACT_URL = "/webservice/contact.ws.php";
	private static final String QSTN = "?";
	private static final String AK = "ak=";
	private static final String API_KEY = "4c6e3cf74458f";
	
	//Contact required fields
	private static final String FIRST_NAME = "first_name";
	private static final String LAST_NAME = "last_name";
	private static final String EMAIL = "email";
	private static final String MOBILE_PHONE = "mobile_phone";
	private static final String GENDER = "gender";
	private static final String DOB = "dob";
	private static final String FK_REGIONS = "fk_regions";
	private static final String FK_CHANNEL_TYPES = "fk_channel_types";
	private static final String BRAND_ID = "brand_id";
	
	private Contact myContact;
	private Context myContext;
	HttpPost post;
	String contactUrlParameters;
	String results;
	
	public SweetSpotApi(Context context, Contact contact) {
		this.myContact = contact;
		this.myContext = context;
		this.results = null;
	}
	
	public static String getPostUrl() {
		return BASE_URL + CONTACT_URL + QSTN + AK + API_KEY;
	}	
	
	public String serializeContact(Contact myContact) {
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair(FIRST_NAME, myContact.getName()));
		pairs.add(new BasicNameValuePair(LAST_NAME, myContact.getSurname()));
		pairs.add(new BasicNameValuePair(EMAIL, myContact.getEmail()));
		pairs.add(new BasicNameValuePair(MOBILE_PHONE, myContact.getMobile()));
		pairs.add(new BasicNameValuePair(DOB, myContact.getDob()));
		pairs.add(new BasicNameValuePair(GENDER, myContact.getGender()));
		pairs.add(new BasicNameValuePair(FK_REGIONS, myContact.getRegion()));
		pairs.add(new BasicNameValuePair(FK_CHANNEL_TYPES, "1"));
		pairs.add(new BasicNameValuePair(BRAND_ID, "21"));
		
		//Implode
		String listString = "";
		for (NameValuePair s : pairs) {
		    listString += s + "&";
		}
		Log.i("Liquorice", "listString"+ pairs); 		
		Log.i("Liquorice", "listString"+listString); 
		return listString;		 
	}
	
	@Override
    protected String doInBackground(String... params) { 
       String urlString=params[0]; // URL to call        
       String postResult = ""; 
       this.results = "";
       // HTTP Get
       try { 
    	   postResult = excutePost(urlString, serializeContact(this.myContact));
        } catch (Exception e ) { 
           Log.i("Liquorice", e.getMessage()); 
           return e.getMessage(); 
        }
        return postResult;      
     }
	
	/**
	 * 
	 * @param targetURL
	 * @param urlParameters
	 * @return
	 */
	public static String excutePost(String targetURL, String urlParameters) {
	  URL url;
	  HttpURLConnection connection = null;  
	  try {
		//Create connection
		url = new URL(targetURL);
		connection = (HttpURLConnection)url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			
		connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
		connection.setRequestProperty("Content-Language", "en-US");  
			
		connection.setUseCaches (false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		
		//Send request
		DataOutputStream wr = new DataOutputStream (connection.getOutputStream ());
		wr.writeBytes (urlParameters);
		wr.flush ();
		wr.close ();
		
		//Get Response	
		InputStream is = connection.getInputStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		String line;
		StringBuffer response = new StringBuffer(); 
		while((line = rd.readLine()) != null) {
		  response.append(line);
		  response.append('\r');
		}
		rd.close();
		return response.toString();

      } catch (Exception e) {
        e.printStackTrace();
        return null;

      } finally {

        if(connection != null) {
          connection.disconnect(); 
        }
      }      
    }
 
    protected void onPostExecute(String result) {
       	//JSON String
       Log.i("Liquorice", "postResulta: "+ result);
       boolean success = false;
       
       if(result != null) {
    	   JSONObject json;           
           try {
        	   json = new JSONObject(result);
        	   JSONObject contact = (JSONObject) json.getJSONObject("contact");
        	   success = (boolean) contact.getBoolean("success");
        	   
           } catch (JSONException e) {
    			e.printStackTrace();
           }
       }      
       Log.i("Liquorice", "success: "+  success);
       //delete from db when success or add to db if not
       if(success) {
    	   //delete contact contact to db
    	   int affectedRows = (int) new DatabaseHandler(this.myContext).deleteContact(this.myContact);   
    	   Log.i("Liquorice", "affectedRows: "+ affectedRows);
       } else {
    	   int id = (int) new DatabaseHandler(this.myContext).addContact(this.myContact);
    	   Log.i("Liquorice", "id: "+ id);
       }
    }
}
	