package com.liquorice.smarte;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.util.Log;

public class ContactSendService extends IntentService {
	
	public ContactSendService() {
		super("ContactSendService");
	}
	public ContactSendService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}	
	 /**
	   * The IntentService calls this method from the default worker thread with
	   * the intent that started the service. When this method returns, IntentService
	   * stops the service, as appropriate.
	   */

	  protected void onHandleIntent(Intent intent) {
	      Context context = getBaseContext();
	      Contact[] contacts = new DatabaseHandler(context).findAllContactObjects();
	      if(contacts != null) {
	    	  //do the sync
	    	  for(Contact contact : contacts) {
	            	Log.i("Liquorice","test"+ contact.getDob());
	            	new SweetSpotApi(context, contact).execute(SweetSpotApi.getPostUrl());
	            } 	    	  
	      }
	      //stop the service afterwards
	      stopSelf();
	  }	
}
