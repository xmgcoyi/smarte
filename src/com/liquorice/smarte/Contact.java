package com.liquorice.smarte;

import android.os.Parcel;
import android.os.Parcelable;



public class Contact implements Parcelable {
    
   //private variables
   int _id;
   String _name;
   String _surname;
   String _mobile;
   String _dob;
   String[] _dobArray;
   String _dobDd;
   String _dobMm;
   String _dobYyyy;
   String _email;
   String _gender;
   String _region;
   
   public static final String KEY_NAME = "name";
   public static final String KEY_SURNAME = "surname";
   public static final String KEY_EMAIL = "email";
   public static final String KEY_MOBILE = "mobile";
   public static final String KEY_DOB = "dob";
   public static final String KEY_GENDER = "gender";
   public static final String KEY_REGION = "region";
   
   // Empty constructor
   public Contact(){
        
   }

   // constructor
   public Contact(int id, String _name, String _surname, String _email, String _mobile, String _dob, String _gender, String _region) {
       this._id = id;
       this._name = _name;
       this._surname = _surname;
       this._mobile = _mobile;
       this._dob = _dob;
       this._email = _email;
       this._gender = _gender;
       this._region = _region;
   }
    
   // constructor
   public Contact(String _name, String _surname,  String _email, String _mobile, String _dob, String _gender, String _region) {
       this._name = _name;
       this._surname = _surname;
       this._mobile = _mobile;
       this._dob = _dob;
       this._email = _email;
       this._gender = _gender;
       this._region = _region;
   } 
   
   public static final String formatDateOfBirth(String yyyy, String mm, String dd) {
	   return yyyy+"-"+mm+"-"+dd;
   }
   
   //setters and getters
	public String getName() {
		return this._name;
	}
	public String getSurname() {
		return this._surname;
	}
	public String getEmail() {
		return this._email;
	}
	public String getMobile() {
		// TODO Auto-generated method stub
		return this._mobile;
	}
	public String getDob() {
		return this._dob;
	}
	public String getDobDd() {
		// TODO Auto-generated method stub
		return this._dobDd;
	}
	public String getDobMm() {
		// TODO Auto-generated method stub
		return this._dobMm;
	}
	public String getDobYyyy() {
		return this._dobYyyy;
	}
	
	public String getGender() {
		return this._gender;
	}
	
	public String getRegion() {
		return this._region;
	}
	
	//setters
	public void setDobDd(String dd) {
		this._dobDd = dd;
	}
	public void setDobMm(String mm) {
		this._dobMm = mm;
	}
	public void setDobYyyy(String yyyy) {
		this._dobYyyy = yyyy;
	}
	

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString( _name);
		dest.writeString(_surname);
		dest.writeString(_mobile);
		dest.writeString(_dob);
		dest.writeString(_email);
		dest.writeString(_gender);
		dest.writeString(_region);
		dest.writeString(_dobDd);
		dest.writeString(_dobMm);
		dest.writeString(_dobYyyy);
		// TODO Auto-generated method stub		
	}


    public Contact(Parcel in) {
    	 this._name = in.readString();
         this._surname = in.readString();
         this._mobile = in.readString();
         this._dob = in.readString();
         this._email = in.readString();
         this._gender = in.readString();
         this._region = in.readString();
    }
    
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public Contact createFromParcel(Parcel in) {
        	return new Contact(in);
        }

        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };

}
