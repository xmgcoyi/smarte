package com.liquorice.smarte;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ThankYouActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_thank_you);
	}
	
	public void mainMenuButtonClick(View view) {
		Intent intent = new Intent(this, NewConsumerActivity.class);
		this.startActivity(intent);
	}
}
