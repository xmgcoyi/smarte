package com.liquorice.smarte;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class SmartE extends Activity  {
	public static final String LIQUORICE = "Liquorice:";
	//static variables
	private static final String INITIAL_DD = "DD";
	private static final String INITIAL_MM = "MM";
	private static final String INITIAL_YYYY = "YYYY";
	private static final int SPINNER_FIRST_ITEM_POSITION = 0;
	private static final String LEADING_ZERO_FORMAT = "%02d";
	private static final int LEGAL_DRINKING_AGE = 18;
	private static final int MINIMUM_MOBILE_LENTGH = 10;
	
	DatabaseHandler db;

	Button submitButton;
	EditText nameEditText;
	EditText surnameEditText;
	EditText emailEditText;
	EditText mobileEditText;
	EditText dobEditText;	
	Spinner ddSpinner;
	Spinner mmSpinner;
	Spinner yyyySpinner;
	Spinner genderSpinner;
	Spinner regionSpinner;
	
	String nameString;
	String surnameString;
	String emailString;
	String mobileString;
	String ddString;
	String mmString;
	String yyyyString;
	String genderString;
	String regionString;
	NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smart_e);       
        
        //We not resuming the app
        if(savedInstanceState == null) {
        	//initiate
        	//initialize the fields
            submitButton = (Button) findViewById(R.id.btnSubmit);
            nameEditText = (EditText) findViewById(R.id.txtName);
            surnameEditText = (EditText) findViewById(R.id.txtSurname);
            emailEditText = (EditText) findViewById(R.id.txtEmail);
            mobileEditText = (EditText) findViewById(R.id.txtMobile);
            db = new DatabaseHandler(this); 
            
            ddSpinner = initDateOfBirthSpinner(R.id.spinnerDD);
            mmSpinner = initDateOfBirthSpinner(R.id.spinnerMM);
            yyyySpinner = initDateOfBirthSpinner(R.id.spinnerYYYY);  
            
            startNetworkConnectivityListener();
            populateRegionSpinner();
            populateGenderSpinner(); 
        }  
    }
    
    private void populateRegionSpinner() {
    	regionSpinner = (Spinner) findViewById(R.id.spinnerRegion);
    	ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.regions_array, android.R.layout.simple_spinner_item);
    	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	regionSpinner.setAdapter(adapter);
    }
    
    private void populateGenderSpinner() {
    	genderSpinner = (Spinner) findViewById(R.id.spinnerGender);
    	ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.genders_array, android.R.layout.simple_spinner_item);
    	adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	genderSpinner.setAdapter(adapter);
    }

//    public void populateSpinner(int spinnderID) {
//        ddSpinner = (Spinner) findViewById(R.id.spinnerDD);
//        mmSpinner = (Spinner) findViewById(R.id.spinnerMM);
//        yyyySpinner = (Spinner) findViewById(R.id.spinnerYYYY);

        
//        nameString = "";
//        surnameString = "";
//    	emailString = "";
//    	mobileString = "";
    	
//        dobEditText = (EditText) findViewById(R.id.txtDob);
//        db = new DatabaseHandler(this); 
        
//    }
    
    private void startNetworkConnectivityListener() {
	        IntentFilter filter = new IntentFilter();
	        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
	        registerReceiver(networkStateReceiver, filter);
	}
    
    public void submitContactButtonClick(View view) {
    	nameString = nameEditText.getText().toString().trim(); 
    	surnameString = surnameEditText.getText().toString().trim();
    	emailString = emailEditText.getText().toString().trim(); 
    	mobileString = mobileEditText.getText().toString().trim();
    	genderString = genderSpinner.getSelectedItem().toString();
    	regionString = regionSpinner.getSelectedItem().toString();
    	ddString = ddSpinner.getSelectedItem().toString();
    	mmString = mmSpinner.getSelectedItem().toString();
    	yyyyString = yyyySpinner.getSelectedItem().toString();

    	if(validateForm()) {   
    		Contact myContact = new Contact(
        			nameString, 
        			surnameString, 	
        			emailString, 
        			mobileString, 
        			Contact.formatDateOfBirth(yyyyString, mmString, ddString),
        			genderString,
        			regionString
    		);
    		
    		Intent i = new Intent(getBaseContext(), ConfirmDetailsActivity.class);
    		i.putExtra("contact", myContact);
    		i.putExtra("dd", ddString);
    		i.putExtra("mm", mmString);
    		i.putExtra("yyyy", yyyyString);    		
    		startActivity(i);
    		
//    		
//			//send details only when connected
//			if(isConnected()) {										
//				new SweetSpotApi(getBaseContext(), myContact).execute(SweetSpotApi.getPostUrl());
//			}else {
//				int id = db.addContact(myContact);
//			} 
    	}
	}
    
    private boolean validateForm() {    	
    	// Reset errors.
			nameEditText.setError(null);
			surnameEditText.setError(null);
			emailEditText.setError(null);
			mobileEditText.setError(null);
			Log.i("Liquorice", "hello there selected dd spinner: " + ddSpinner.getSelectedItem().toString());
			boolean valid = true;

			// Name validation
			if (TextUtils.isEmpty(nameString)) {
				nameEditText.setError("Name may not be empty");
				valid = false;
			} else if (!nameString.matches("[a-zA-Z_0-9]+")) {
				nameEditText.setError("Please enter valid name");
				valid = false;
			}
			
			//Surname validation
			if (TextUtils.isEmpty(surnameString)) {
				surnameEditText.setError("Surname may not be empty");
				valid = false;
			} else if (!surnameString.matches("[a-zA-Z_0-9]+")) {
				surnameEditText.setError("Please enter valid surname");
				valid = false;
			}
			
			//validate gender
			if(genderSpinner.getSelectedItemPosition() == SPINNER_FIRST_ITEM_POSITION) {
				Toast.makeText(this, "Please choose Gender", Toast.LENGTH_LONG).show();
				valid = false;
			}
			
			//validate region
			if(genderSpinner.getSelectedItemPosition() == SPINNER_FIRST_ITEM_POSITION) {
				Toast.makeText(this, "Please choose Region", Toast.LENGTH_LONG).show();
				valid = false;
			}
			//either email or mobile must be present
			if(TextUtils.isEmpty(emailString) && TextUtils.isEmpty(mobileString)) {
				Toast.makeText(this, "Email or Mobile is required", Toast.LENGTH_LONG).show();
				valid = false;
			}
			
			//valid email
			if (!TextUtils.isEmpty(emailString) && !android.util.Patterns.EMAIL_ADDRESS.matcher(emailString).matches()) {
				emailEditText.setError("Email is not valid");
				valid = false;
			}
			
			//valid mobile
			if (!TextUtils.isEmpty(mobileString)) {
				if(!TextUtils.isDigitsOnly(mobileString)){
					mobileEditText.setError("Mobile must be digits only");
					valid = false;
				}else if(mobileString.length() < MINIMUM_MOBILE_LENTGH) {
					mobileEditText.setError("Mobile must is 10 digits minimum");
					valid = false;
				}
			}
			
			//all DOB spinners must be valid
			if (!validateDobSpinners(ddSpinner) || !validateDobSpinners(mmSpinner)  || !validateDobSpinners(yyyySpinner) ) {				
				valid = false;
			}else {
				int age = getAge(Integer.parseInt(yyyySpinner.getSelectedItem().toString()), 
						Integer.parseInt(mmSpinner.getSelectedItem().toString()), 
								Integer.parseInt(ddSpinner.getSelectedItem().toString()));
				//check drinking age
				if(age < LEGAL_DRINKING_AGE) {
					Toast.makeText(this, "Under Age", Toast.LENGTH_LONG).show();
					valid = false;
					Log.i("Liquorice", "less than drinking age: ");
				}
				Log.i("Liquorice", "age: " + age);
			}
			
			return valid;
    }
    
    private boolean validateDobSpinners(Spinner spinner) {
    	String initial_string = "";
    	String msg = "";
    	boolean valid = true;

    	switch(spinner.getId()) {
    		case R.id.spinnerDD :
    			initial_string = INITIAL_DD;
    			msg = "Please select day";
    			break;
    		case R.id.spinnerMM :
    			initial_string = INITIAL_MM;
    			msg = "Please select month";
    			break;
    		case R.id.spinnerYYYY :
    			initial_string = INITIAL_YYYY;
    			msg = "Please select year";
    			break;
    	}    	
    	//mobile validation
		if (TextUtils.equals(spinner.getSelectedItem().toString(), initial_string) ) {
			Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
			valid = false;
		}
		return valid;
    }
    
    /**
     * Method to extract the user's age from the entered Date of Birth.
     * 
     * @param DoB String The user's date of birth.
     * 
     * @return ageS String The user's age in years based on the supplied DoB.
     */
    private int getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day); 

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--; 
        }
        return age;  
    }
    
    private Spinner initDateOfBirthSpinner(int spinnderID) {
    	String firstItem = "";
    	int startNumber = 0;
    	int maxNumber = 0;
    	
    	switch(spinnderID) {
	    	case R.id.spinnerDD:
	    		firstItem = INITIAL_DD;
	    		maxNumber = 31;  
	    		break;
	    	case R.id.spinnerMM:
	    		firstItem = INITIAL_MM;
	    		maxNumber = 12;  
	    		break;
	    	case R.id.spinnerYYYY:
	    		firstItem = INITIAL_YYYY;
	    		startNumber = 1900;
	    		maxNumber = 2014;  
	    		break;
    	}
    	
    	//populate the spinner
    	List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add(firstItem);
        for(int i = startNumber; i < maxNumber; i++) {
    		spinnerArray.add(String.format(LEADING_ZERO_FORMAT, i+1));
    	}
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
       
        Spinner sItems = (Spinner) findViewById(spinnderID);
        sItems.setAdapter(adapter);
        return sItems;
    }  
}
